[![pipeline status](https://gitlab.com/seaofvoices/darklua/badges/main/pipeline.svg)](https://gitlab.com/seaofvoices/darklua/commits/main)
[![version](https://img.shields.io/crates/v/darklua)](https://crates.io/crates/darklua)
[![license](https://img.shields.io/crates/l/darklua)](LICENSE.txt)

# darklua

Transform Lua 5.1 and Roblox Lua scripts using rules.

# [Documentation](https://darklua.com/docs)

Visit https://darklua.com/docs to learn how to use darklua.

# [Try It!](https://darklua.com/try-it)

You can try darklua directly into your browser! Check out https://darklua.com/try-it.

# License

darklua is available under the MIT license. See [LICENSE.txt](LICENSE.txt) for details.
